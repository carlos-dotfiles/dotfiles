" disable vi compatibility (emulation of old bugs)
set nocompatible

" Project specific vimrc file
set exrc

"""""""""""""
"" PLUGINS ""
"""""""""""""

call plug#begin()
" The default plugin directory will be as follows:
"   - '~/.vim/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" NerdTree: file system explorer
Plug 'scrooloose/nerdtree'
" Surround.vim: Make surrounding text easier
Plug 'tpope/vim-surround'
" Make repeating easy with '.' with plugins
Plug 'tpope/vim-repeat'
" Git wrapper
Plug 'tpope/vim-fugitive'
" Git Gutter info
Plug 'airblade/vim-gitgutter'
" Multiple Curson with Ctrl-n
Plug 'terryma/vim-multiple-cursors'
" File finder
Plug 'ctrlpvim/ctrlp.vim'
" Emmet for html
Plug 'mattn/emmet-vim'
" Syntax helper
Plug 'dense-analysis/ale'
" Easily move in line
Plug 'easymotion/vim-easymotion'
" Use tab for completion
Plug 'ervandew/supertab'
" Snippet engine.
Plug 'SirVer/ultisnips'
" Automatic Paste
Plug 'roxma/vim-paste-easy'

" Lenguage Support Plugins: 
" Javascript
Plug 'pangloss/vim-javascript'
" Python
Plug 'nvie/vim-flake8'

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting

"""""""""""""""""""""""""""""""
"" PLUGIN & LENGUAGE OPTIONS ""
"""""""""""""""""""""""""""""""

" Make CTRL+P Ignore certain directories
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'

" SuperTab
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:SuperTabCrMapping             = 0

" UltiSnips
let g:UltiSnipsExpandTrigger        = '<tab>'
let g:UltiSnipsJumpForwardTrigger   = '<tab>'
let g:UltiSnipsJumpBackwardTrigger  = '<s-tab>'
let g:UltiSnipsEditSplit            = "tabdo"
let g:UltiSnipsSnippetsDir          = $HOME."/UltiSnips"
let g:UltiSnipsSnippetDirectories   = [$HOME."/UltiSnips", "UltiSnips"]

" Javascript
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1

""""""""""""""""
"" FORMATTING ""
""""""""""""""""
" Set encoding
set encoding=utf-8
set fenc=utf-8
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
set term=xterm-256color
set termencoding=utf-8

" configure tabwidth and insert spaces instead of tabs
set tabstop=2        " tab width is 2 spaces 
set shiftwidth=2     " indent also with 2 spaces
set expandtab        " expand tabs to spaces
set smartindent      " Smart Indent

" Special configuration of 4 spaces
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set fileformat=unix

" dont wrap lines phisicaly but visually
set textwidth=0 wrapmargin=0
set wrap
set linebreak
set nolist

"""""""""""""
"" GENERAL ""
"""""""""""""

" turn syntax highlighting on
syntax on

" Use Dark Background
set background=dark

" Syntax only first 400 characters in line
set synmaxcol=400

" turn line numbers on
set number relativenumber

" highlight matching braces
set showmatch

" Sepparate Word by other characters
set iskeyword+=!-~,^*,^123-125,192-255,^_,^92,^61,^33-47,^58

" Ignore case with search
set ignorecase

" Use Bash as shell
set shell=/bin/bash

" Show command you are typing
set showcmd

" Add Line on cursor position
set cursorline

" Fold method and options
set foldmethod=indent
set foldopen-=block
set foldlevel=99

"""""""""""""
"" BIDINGS ""
"""""""""""""

" Make Space Leader key
let mapleader = " "
inoremap jj <ESC>

"""""""""""""""""
"" STATUS LINE ""
"""""""""""""""""

" Make Status line always Visible
set laststatus=2

" File
set statusline=%f
" FileType
set statusline+=\ %y
" current line and column to right
set statusline+=\ %=%(%l,%c\ %)

" set secure for external files
set secure
